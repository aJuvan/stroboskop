# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://aJuvan@bitbucket.org/aJuvan/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/aJuvan/stroboskop/commits/d26da293749708967309ced8fd74acc2f789dd0f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aJuvan/stroboskop/commits/1f381dc8ab7c08a66712ed6ce1cb5a155d0b3b71

Naloga 6.3.2:
https://bitbucket.org/aJuvan/stroboskop/commits/4ec6ea7b3d2d35d541fe6b2fa8982813ad7c35c9

Naloga 6.3.3:
https://bitbucket.org/aJuvan/stroboskop/commits/c2a4f23ef02b5577071864d82db91fb7d42af5d5

Naloga 6.3.4:
https://bitbucket.org/aJuvan/stroboskop/commits/4628e53e7b491a3c8343b292b9468bddc9aed8a8

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aJuvan/stroboskop/commits/2f3cf3a6dc231d4ec9188cd7168527eac3db7092

Naloga 6.4.2:
https://bitbucket.org/aJuvan/stroboskop/commits/cb69142980b0e2443ab05fc724eacf9fa0bfe038

Naloga 6.4.3:
https://bitbucket.org/aJuvan/stroboskop/commits/0594fedf03b670f3098dfe6cb4dacaaac1ff74ca

Naloga 6.4.4:
https://bitbucket.org/aJuvan/stroboskop/commits/270ff420ae4e8eeb923d7aa5d05383b8a76bcef5